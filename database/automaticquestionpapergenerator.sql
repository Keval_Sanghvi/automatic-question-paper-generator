-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2021 at 04:02 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automaticquestionpapergenerator`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers_mcq`
--

CREATE TABLE `answers_mcq` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_value` varchar(255) NOT NULL,
  `isAnswer` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers_mcq`
--

INSERT INTO `answers_mcq` (`id`, `question_id`, `option_value`, `isAnswer`) VALUES
(54, 26, 'tag', 1),
(55, 26, 'variable', 0),
(56, 26, 'element', 0),
(57, 26, 'keyword', 0),
(58, 27, '1', 0),
(59, 27, '1/2', 1),
(60, 27, '-1/2', 0),
(61, 27, '0', 0),
(62, 30, 'state', 0),
(63, 30, 'static', 1),
(64, 30, 'extend', 0),
(65, 30, 'constructor', 0),
(66, 32, 'Zac Mills', 0),
(67, 32, 'Kristen Jack', 0),
(68, 32, 'Guido van Rossum', 1),
(69, 32, 'David Smith', 0),
(70, 33, '1991', 0),
(71, 33, '1995', 0),
(72, 33, '1978', 0),
(73, 33, '1989', 1),
(74, 34, '.p', 0),
(75, 34, '.ipynb', 0),
(76, 34, '.py', 1),
(77, 34, 'none of these', 0),
(78, 35, '//', 0),
(79, 35, '\\\\', 0),
(80, 35, '#', 1),
(81, 35, '!', 0),
(82, 36, 'val', 1),
(83, 36, 'with', 0),
(84, 36, 'raise', 0),
(85, 36, 'try', 0),
(86, 38, 'React', 0),
(87, 38, 'Vue', 0),
(88, 38, 'Swing', 1),
(89, 38, 'Angular', 0),
(90, 39, 'java.awt', 1),
(91, 39, 'java.core.awt', 0),
(92, 39, 'java.adv.awt', 0),
(93, 39, 'java.awt.awt', 0),
(94, 47, 'Java Diagnose Kit', 0),
(95, 47, 'Java Development Kit', 1),
(96, 47, 'Java Doc Kit', 0),
(97, 47, 'Java Documentation Kit', 0),
(98, 53, 'java.awt.Container', 0),
(99, 53, 'java.awt.com.Component', 0),
(100, 53, 'java.awt.Component', 1),
(101, 53, 'java.awt.AWT', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `subject_id`, `chapter_name`) VALUES
(4, 1, 'AWT'),
(16, 1, 'jsp'),
(1, 1, 'oops concepts'),
(17, 1, 'swing'),
(2, 2, 'Basics'),
(6, 2, 'Tkinter'),
(13, 12, 'javascript'),
(14, 13, 'trigonometry');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `isMCQType` tinyint(1) NOT NULL,
  `marks` int(11) NOT NULL,
  `difficulty` int(11) NOT NULL,
  `probability` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `subject_id`, `chapter_id`, `question`, `isMCQType`, `marks`, `difficulty`, `probability`) VALUES
(20, 1, 1, 'What is static?', 0, 2, 2, 2),
(21, 1, 4, 'AWT is -', 0, 1, 1, 0),
(22, 2, 2, 'python is compiled or interpreted?', 0, 1, 1, 0),
(24, 12, 13, 'what are the different data types present in javascript?', 0, 3, 2, 0),
(26, 12, 13, 'what is <script> in js ?', 1, 1, 2, 0),
(27, 13, 14, 'sin(30) = ?', 1, 1, 1, 0),
(28, 1, 1, 'explain method overloading?', 0, 3, 2, 2),
(29, 1, 1, 'describe checked and unchecked exceptions', 0, 2, 2, 2),
(30, 1, 1, 'which of the following is a keyword?', 1, 1, 1, 0),
(31, 2, 2, 'Give difference between list and tuple.', 0, 3, 3, 0),
(32, 2, 2, 'Who developed the Python language?', 1, 1, 2, 0),
(33, 2, 2, 'In which year was the Python language developed?', 1, 1, 1, 0),
(34, 2, 2, 'Which one of the following is the correct extension of the Python file?', 1, 2, 2, 0),
(35, 2, 2, 'Which character is used in Python to make a single line comment?', 1, 1, 1, 0),
(36, 2, 2, 'Which of the following is not a keyword in Python language?', 1, 1, 2, 0),
(37, 2, 2, 'Give advantages of Python language.', 0, 5, 2, 0),
(38, 12, 13, 'Which of these is not a Javascript framework or a library?', 1, 1, 1, 0),
(39, 1, 4, 'AWT is found in which package?', 1, 1, 2, 2),
(41, 2, 6, 'Explain pack(),  grid() and place() in Tkinter.', 0, 6, 3, 0),
(42, 13, 14, 'Give all values for sin from 0 to 180', 0, 2, 2, 0),
(43, 1, 1, 'Explain exception handling.', 0, 2, 2, 2),
(44, 1, 1, 'Explain JDK', 0, 3, 2, 2),
(45, 1, 1, 'Explain JRE', 0, 3, 2, 2),
(46, 1, 1, 'Explain JVM', 0, 3, 2, 2),
(47, 1, 1, 'Full Form of JDK', 1, 1, 1, 0),
(48, 1, 1, 'What are wrapper classes in Java?', 0, 3, 2, 2),
(49, 1, 1, 'Explain public static void main(String args[]) in Java.', 0, 2, 3, 0),
(50, 1, 17, 'What is difference between AWT and Swing?', 0, 3, 2, 2),
(51, 1, 17, 'Why Swing components are called lightweight component?', 0, 2, 1, 0),
(52, 1, 17, 'What is Event Driven Thread (EDT) is Swing?', 0, 2, 3, 0),
(53, 1, 4, 'What is the super class of all components of Java?', 1, 1, 1, 0),
(54, 1, 4, 'What are the default layout managers for containers?', 0, 2, 2, 2),
(55, 1, 1, 'Explain Dynamic method dispatching.', 0, 4, 3, 0),
(56, 1, 1, 'Explain different types of inheritance in JAVA', 0, 4, 3, 0),
(57, 1, 1, 'How to create a Thread in java?', 0, 3, 2, 2),
(58, 1, 4, 'What is a layout manager?', 0, 2, 2, 2),
(59, 1, 1, 'What is the difference between equals() and == in Java?', 0, 4, 2, 1),
(60, 1, 1, 'What is a package in Java? List down various advantages of packages.', 0, 5, 2, 1),
(61, 1, 1, 'Why pointers are not used in Java?', 0, 2, 2, 1),
(62, 1, 4, 'What is difference between invokeAndWait and invokeLater in Java?  Read more: https://www.java67.com/2013/01/10-awt-swing-interview-questions-answers-java.html#ixzz6xUEexFJX', 0, 2, 2, 2),
(63, 1, 4, 'How to change a button from enable to disable after click ?', 0, 3, 2, 1),
(64, 1, 4, 'What is difference between Container and Component ?', 0, 3, 2, 1),
(65, 1, 17, 'Which method of Swing are thread-safe?', 0, 3, 2, 2),
(66, 1, 1, 'Is Java pure object oriented Language? Explain.', 0, 5, 2, 1),
(67, 1, 17, 'Why is Model-View-Controller Architecture used in Swing?', 0, 3, 2, 1),
(68, 1, 17, 'What is the difference between the paint() and repaint() methods?', 0, 2, 2, 1),
(69, 1, 17, 'Why does JComponent have add() and remove() methods but Component does not?', 0, 4, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_name`) VALUES
(1, 'Java'),
(13, 'Maths'),
(2, 'Python'),
(16, 'RDBMS'),
(12, 'Web Development');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers_mcq`
--
ALTER TABLE `answers_mcq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id_fk` (`question_id`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subject_id` (`subject_id`,`chapter_name`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chapter_id_fk` (`chapter_id`),
  ADD KEY `subject_id_fk` (`subject_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_subject` (`subject_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers_mcq`
--
ALTER TABLE `answers_mcq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers_mcq`
--
ALTER TABLE `answers_mcq`
  ADD CONSTRAINT `question_id_fk` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chapters`
--
ALTER TABLE `chapters`
  ADD CONSTRAINT `chapters_ibfk_1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `chapter_id_fk` FOREIGN KEY (`chapter_id`) REFERENCES `chapters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subject_id_fk` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
