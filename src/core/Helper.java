/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Pattern;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;

/**
 *
 * @author Keval Sanghvi
 */
public class Helper {
    
    private static Pattern pattern = Pattern.compile("\\d+(\\.\\d+)?");
    
    public static void setJList(HashMap map, JList list) {
        DefaultListModel listModel = new DefaultListModel();
        Collection hashMapValues = map.values();
        Object[] hashMapArray = hashMapValues.toArray();
        for (int i = 0; i < map.size(); i++) {
            listModel.addElement(hashMapArray[i]);
        }
        list.setModel(listModel);
    }
    
    public static int getKeyFromValue(HashMap<Integer, String> map, String value) {
        for (Integer id : map.keySet()) {
            if (map.get(id).equals(value)) {
              return id;
            }
        }
        return -1;
    }
    
    public static String getSelectedButtonText(ButtonGroup buttonGroup) {
        Enumeration<AbstractButton> buttons = buttonGroup.getElements();
        while(buttons.hasMoreElements()) {
            AbstractButton button = buttons.nextElement();
            if (button.isSelected()) {
                return button.getText();
            }
        }
        return null;
    }
    
    public static void getAllSubjects(HashMap<Integer, String> subjectsMap, JList<String> list) {
        ResultSet rs = Database.select("SELECT * FROM subjects");
        try {
            while(rs.next()) {
                subjectsMap.put(rs.getInt(1), rs.getString(2));
            }
            setJList(subjectsMap, list);
        } catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
        }
    }
    
    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false; 
        }
        return pattern.matcher(strNum).matches();
    }
}
