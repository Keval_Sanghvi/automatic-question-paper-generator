import core.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.sql.*;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Keval Sanghvi
 */
public class PDFCreator {
    private static final String FILE = "c:/temp/questionPaper.pdf";
    private static final Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static final Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
    private static final Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static final Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private ArrayList<Question> questions;
    String subjectName;
    int totalMarks;
    PDFCreator(ArrayList<Question> questions, int totalMarks, String subjectName) {
        this.questions = questions;
        this.totalMarks = totalMarks;
        this.subjectName = subjectName;
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            addMetaData(document);
            addTitlePage(document);
            addContent(document);
            addFooterPage(document);
            document.close();
        } catch(Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    private void addMetaData(Document document) {
        document.addTitle("Question Paper");
    }
    
    private void addTitlePage(Document document) throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("Subject Name: " + subjectName.toUpperCase(), catFont));
        if(totalMarks > 100){
            preface.add(new Paragraph("Paper Duration: 2hours", smallBold));
        } else if(totalMarks < 100 && totalMarks > 30){
            preface.add(new Paragraph("Paper Duration: 1hours", smallBold));
        } else {
            preface.add(new Paragraph("Paper Duration: 30 minutes", smallBold));
        }
        preface.add(new Paragraph("Total Marks: " + totalMarks, smallBold));
        preface.add(new Paragraph("Date: " + LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), smallBold));
        preface.add(new Paragraph("_______________________________________________________________________________________", smallBold));
        addEmptyLine(preface,1);
        document.add(preface);
    }

    private void addContent(Document document) throws DocumentException {
        Paragraph paragraph = new Paragraph();
        createTable(paragraph);
        document.add(paragraph);
    }

    private void createTable(Paragraph paragraph) throws BadElementException {
        try {
            PdfPTable table = new PdfPTable(3);
            table.setWidths(new int[]{40, 220, 40});
            PdfPCell c = new PdfPCell(new Phrase("Sr No."));
            c.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c);
            c = new PdfPCell(new Phrase("Question"));
            c.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c);
            c = new PdfPCell(new Phrase("Marks"));
            c.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c);
            table.setHeaderRows(1);
            for(int i = 0; i < questions.size(); i++) {
                PdfPCell a = new PdfPCell(new Phrase(i + 1 + ""));
                a.setPaddingTop(10);
                a.setPaddingBottom(10);
                a.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(a);
                if(questions.get(i).getIsMCQ()) {
                    Phrase p = new Phrase(questions.get(i).getQuestion());
                    ResultSet rs = Database.select("SELECT option_value FROM answers_mcq", "question_id", "=", Integer.toString(questions.get(i).getId()));
                    int j = 1;
                    while(rs.next()) {
                        p.add("\n\n" + j + ") " + rs.getString(1));
                        j++;
                    }
                    PdfPCell m = new PdfPCell(p);
                    m.setPaddingTop(10);
                    m.setPaddingBottom(10);
                    table.addCell(m);
                } else {
                    PdfPCell m = new PdfPCell(new Phrase(questions.get(i).getQuestion()));
                    m.setPaddingTop(10);
                    m.setPaddingBottom(10);
                    table.addCell(m);
                }
                PdfPCell b = new PdfPCell(new Phrase(questions.get(i).getMarks() + ""));
                b.setPaddingTop(10);
                b.setPaddingBottom(10);
                b.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(b);
            }
            paragraph.add(table);
        } catch(Exception e){
            System.out.println(e);
        }
    }
    
    public void addFooterPage(Document document) throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("Rules: ", subFont));
        preface.add(new Paragraph("1) Attempt All Questions.", normalFont));
        preface.add(new Paragraph("2) Draw Diagrams Wherever Neccessary.", normalFont));
        preface.add(new Paragraph("3) Do Not Cheat.", normalFont));
        document.add(preface);
    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    
    public static void addTab(Paragraph paragraph, int number){
        String tab = "    ";
        Chunk chunk = new Chunk("");
        for(int i = 0; i < number; i++){
            chunk.append(tab);
            paragraph.add(chunk);
        }
    }
}
