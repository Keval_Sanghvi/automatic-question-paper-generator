import core.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JOptionPane;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Keval Sanghvi
 */
public class UpdateQuestionForm extends javax.swing.JFrame {

    /**
     * Creates new form UpdateQuestionForm
     */
    String question;
    int questionId;
    String marks;
    boolean isMCQ;
    int difficulty;
    public UpdateQuestionForm(String question, int questionId) {
        this.question = question;
        this.questionId = questionId;
        initComponents();
        questionField.setText(question);
        getQuestionDetails();
        marksField.setText(marks);
        if(isMCQ) {
            isMCQTypeButton.setSelected(true);
            getOptions();
            setSize(740, 670);
        } else {
            mcqPanel.setVisible(false);
            setSize(740, 370);
        }
        setDifficultyLevel();
    }
    
    private void setDifficultyLevel() {
        switch (difficulty) {
            case 1 -> easyDifficultyOption.setSelected(true);
            case 2 -> mediumDifficultyOption.setSelected(true);
            case 3 -> hardDifficultyOption.setSelected(true);
        }
    }
    
    private void getOptions() {
        String options[] = new String[4];
        int i = 0;
        int correctOption = 0;
        ResultSet rs = Database.select("SELECT * FROM answers_mcq", "question_id", "=", Integer.toString(questionId));
        try {
            while(rs.next()) {
                options[i++] = rs.getString(3);
                if(rs.getInt(4) == 1) {
                    correctOption = i;
                }
            }
        } catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
        }
        option1.setText(options[0]);
        option2.setText(options[1]);
        option3.setText(options[2]);
        option4.setText(options[3]);
        switch(correctOption) {
            case 1 -> correctAnswer1.setSelected(true);
            case 2 -> correctAnswer2.setSelected(true);
            case 3 -> correctAnswer3.setSelected(true);
            case 4 -> correctAnswer4.setSelected(true);
        }
    }

    private void getQuestionDetails() {
        ResultSet rs = Database.select("SELECT * FROM questions", "id", "=", Integer.toString(questionId));
        try {
            while(rs.next()) {
                marks = rs.getString(6);
                isMCQ = rs.getBoolean(5);
                difficulty = rs.getInt(7);
            }
        } catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        questionField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        isMCQTypeButton = new javax.swing.JCheckBox();
        updateQuestionInDB = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        marksField = new javax.swing.JTextField();
        easyDifficultyOption = new javax.swing.JRadioButton();
        mediumDifficultyOption = new javax.swing.JRadioButton();
        hardDifficultyOption = new javax.swing.JRadioButton();
        mcqPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        option1 = new javax.swing.JTextField();
        option2 = new javax.swing.JTextField();
        option3 = new javax.swing.JTextField();
        option4 = new javax.swing.JTextField();
        correctAnswer1 = new javax.swing.JRadioButton();
        correctAnswer2 = new javax.swing.JRadioButton();
        correctAnswer3 = new javax.swing.JRadioButton();
        correctAnswer4 = new javax.swing.JRadioButton();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("UpdateQuestion");

        jPanel1.setBackground(new java.awt.Color(100, 144, 240));

        questionField.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Question: ");

        isMCQTypeButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        isMCQTypeButton.setText("Is Question of Type MCQ?");
        isMCQTypeButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        isMCQTypeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isMCQTypeButtonActionPerformed(evt);
            }
        });

        updateQuestionInDB.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        updateQuestionInDB.setText("Update Question");
        updateQuestionInDB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        updateQuestionInDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateQuestionInDBActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Marks: ");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Difficulty:");

        marksField.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        buttonGroup2.add(easyDifficultyOption);
        easyDifficultyOption.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        easyDifficultyOption.setText("Easy");
        easyDifficultyOption.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        buttonGroup2.add(mediumDifficultyOption);
        mediumDifficultyOption.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mediumDifficultyOption.setText("Medium");
        mediumDifficultyOption.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        buttonGroup2.add(hardDifficultyOption);
        hardDifficultyOption.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        hardDifficultyOption.setText("Hard");
        hardDifficultyOption.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        mcqPanel.setBackground(new java.awt.Color(100, 144, 240));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Option 1: ");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Option 2:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Option 3:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Option 4:");

        option1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        option2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        option3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        option4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        buttonGroup1.add(correctAnswer1);
        correctAnswer1.setText("option1");

        buttonGroup1.add(correctAnswer2);
        correctAnswer2.setText("option2");

        buttonGroup1.add(correctAnswer3);
        correctAnswer3.setText("option3");

        buttonGroup1.add(correctAnswer4);
        correctAnswer4.setText("option4");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Select Correct Option:");

        javax.swing.GroupLayout mcqPanelLayout = new javax.swing.GroupLayout(mcqPanel);
        mcqPanel.setLayout(mcqPanelLayout);
        mcqPanelLayout.setHorizontalGroup(
            mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcqPanelLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mcqPanelLayout.createSequentialGroup()
                        .addGroup(mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(61, 61, 61)
                        .addGroup(mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(option1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(option2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(option3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(option4, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(mcqPanelLayout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38)
                        .addComponent(correctAnswer1)
                        .addGap(18, 18, 18)
                        .addComponent(correctAnswer2)
                        .addGap(18, 18, 18)
                        .addComponent(correctAnswer3)
                        .addGap(18, 18, 18)
                        .addComponent(correctAnswer4)))
                .addContainerGap(248, Short.MAX_VALUE))
        );
        mcqPanelLayout.setVerticalGroup(
            mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcqPanelLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(option1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(option2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(option3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(option4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addGroup(mcqPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(correctAnswer1)
                    .addComponent(correctAnswer2)
                    .addComponent(correctAnswer3)
                    .addComponent(correctAnswer4))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(35, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(isMCQTypeButton)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(easyDifficultyOption)
                                .addGap(18, 18, 18)
                                .addComponent(mediumDifficultyOption, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(hardDifficultyOption))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(questionField, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                                .addComponent(marksField)))))
                .addGap(24, 24, 24))
            .addComponent(mcqPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(269, 269, 269)
                .addComponent(updateQuestionInDB)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(questionField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(marksField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(easyDifficultyOption)
                    .addComponent(mediumDifficultyOption)
                    .addComponent(hardDifficultyOption))
                .addGap(39, 39, 39)
                .addComponent(isMCQTypeButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mcqPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(updateQuestionInDB)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void isMCQTypeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_isMCQTypeButtonActionPerformed
        // TODO add your handling code here:
        if(isMCQTypeButton.isSelected()) {
            mcqPanel.setVisible(true);
            setSize(740, 670);
        } else {
            mcqPanel.setVisible(false);
            setSize(740, 370);
        }
    }//GEN-LAST:event_isMCQTypeButtonActionPerformed

    private int getDifficultyLevel(String level) {
        return switch (level) {
            case "Easy" -> 1;
            case "Medium" -> 2;
            case "Hard" -> 3;
            default -> -1;
        };
    }
    
    private void updateQuestionInDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateQuestionInDBActionPerformed
        // TODO add your handling code here:
        String questionInput = questionField.getText();
        String marksInput = marksField.getText();
        if(questionInput.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Question cannot be Empty!");
            return;
        }
        if(marksInput.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Marks cannot be Empty!");
            return;
        }
        if(!Helper.isNumeric(marksInput)) {
            JOptionPane.showMessageDialog(this, "Marks cannot contain Alphabets or Symbols!");
            return;
        }
        if(buttonGroup2.getSelection() == null) {
            JOptionPane.showMessageDialog(this, "Select Difficulty for Question!");
            return;
        }
        String difficultyLevel = Helper.getSelectedButtonText(buttonGroup2);
        int isMCQ_integer = isMCQTypeButton.isSelected() ? 1 : 0;
        List<String> colName = new ArrayList<>();
        colName.add("question");
        colName.add("isMCQType");
        colName.add("marks");
        colName.add("difficulty");
        List<String> colVal = new ArrayList<>();
        colVal.add(questionInput);
        colVal.add(Integer.toString(isMCQ_integer));
        colVal.add(marksInput);
        colVal.add(Integer.toString(getDifficultyLevel(difficultyLevel)));
        boolean result = Database.update(colName, colVal, "questions", "id", questionId);
        boolean resultDelete = false;
        if(isMCQ && !isMCQTypeButton.isSelected()) {
            resultDelete = Database.delete("answers_mcq", "question_id", questionId);
        }
        if(isMCQTypeButton.isSelected()) {
            colName.clear();
            colVal.clear();
            String first_option = option1.getText();
            String second_option = option2.getText();
            String third_option = option3.getText();
            String fourth_option = option4.getText();
            if(first_option.isEmpty() || second_option.isEmpty() || third_option.isEmpty() || fourth_option.isEmpty()) {
                JOptionPane.showMessageDialog(this, "Option cannot be empty!");
                return;
            }
            if(buttonGroup1.getSelection() == null) {
                JOptionPane.showMessageDialog(this, "Select correct Answer!");
                return;
            }
            String correctOptionValue = Helper.getSelectedButtonText(buttonGroup1);
            int isAnswerArray[] = new int[4];
            Arrays.fill(isAnswerArray, 0);
            isAnswerArray[Integer.parseInt(String.valueOf(correctOptionValue.charAt(6))) - 1] = 1;
            String optionValues[] = new String[4];
            optionValues[0] = first_option;
            optionValues[1] = second_option;
            optionValues[2] = third_option;
            optionValues[3] = fourth_option;
            colName.add("question_id");
            colName.add("option_value");
            colName.add("isAnswer");
            boolean resultBool[] = new boolean[4];
            int resultInt[] = new int[4];
            int idArray[] = new int[4];
            for(int i = 0; i < 4; i++) {
                colVal.clear();
                colVal.add(Integer.toString(questionId));
                colVal.add(optionValues[i]);
                colVal.add(Integer.toString(isAnswerArray[i]));
                if(isMCQ) {
                    ResultSet rs = Database.select("SELECT * FROM answers_mcq", "question_id", "=", Integer.toString(questionId));
                    int j = 0;
                    try {
                        while(rs.next()) {
                            idArray[j++] = rs.getInt(1); 
                        }
                    } catch(SQLException e) {
                        JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
                    }
                    resultBool[i] = Database.update(colName, colVal, "answers_mcq", "id", idArray[i]);
                } else {
                    resultInt[i] = Database.insert(colName, colVal, "answers_mcq");
                }
            }
            if((result && resultDelete) || (result && !Arrays.asList(resultInt).contains(-1)) || (result && !Arrays.asList(resultBool).contains(false))) {
                JOptionPane.showMessageDialog(this, "Question successfully updated!");
            } else {
                JOptionPane.showMessageDialog(this, "Updation of Question Failed!");
            }
        }
        this.dispose();
    }//GEN-LAST:event_updateQuestionInDBActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JRadioButton correctAnswer1;
    private javax.swing.JRadioButton correctAnswer2;
    private javax.swing.JRadioButton correctAnswer3;
    private javax.swing.JRadioButton correctAnswer4;
    private javax.swing.JRadioButton easyDifficultyOption;
    private javax.swing.JRadioButton hardDifficultyOption;
    private javax.swing.JCheckBox isMCQTypeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField marksField;
    private javax.swing.JPanel mcqPanel;
    private javax.swing.JRadioButton mediumDifficultyOption;
    private javax.swing.JTextField option1;
    private javax.swing.JTextField option2;
    private javax.swing.JTextField option3;
    private javax.swing.JTextField option4;
    private javax.swing.JTextField questionField;
    private javax.swing.JButton updateQuestionInDB;
    // End of variables declaration//GEN-END:variables
}
