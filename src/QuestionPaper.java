import core.*;
import java.util.HashMap;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Keval Sanghvi
 */
public class QuestionPaper {
    HashMap<String, String> weightageValue;
    String subjectName;
    ArrayList<Question> questions;
    int difficultyLevel;
    int selectedMarks = 0;
    int totalMarks = 0;
    ArrayList<Integer> chapterList;
    ResultSet rs;
    QuestionPaper(HashMap<String, String> weightageValue, String subjectName, int difficultyLevel) {
        this.weightageValue = weightageValue;
        this.subjectName = subjectName;
        this.difficultyLevel = difficultyLevel;
        questions = new ArrayList<>();
        chapterList = new ArrayList<>();
        getChapterList();
        int result = getQuestions();
        if(result != -1) {
            int res = setProbability();
            if(res != -1) {
                Collections.sort(questions, new QuestionCompareByMarks());
                new PDFCreator(questions, getTotalMarks(), subjectName);
                JOptionPane.showMessageDialog(null, "Question Paper Generated!");
            }
        }
    }
    
    private int getTotalMarks() {
        int marks = 0;
        for(String key: weightageValue.keySet()) {
            marks += Integer.parseInt(weightageValue.get(key));
        }
        return marks;
    }
    
    private void getChapterList() {
        for(String key: weightageValue.keySet()) {
            rs = Database.select("SELECT id FROM chapters", "chapter_name", "=", "'" + key + "'");
            try {
                while(rs.next()) {
                    chapterList.add(rs.getInt(1));
                }
            } catch(SQLException e) {
                JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
            }
        }
    }
    
    private String getChapterNameFromChapterId(int id) {
        String chapterName = "";
        ResultSet rs = Database.select("SELECT chapter_name FROM chapters", "id", "=", Integer.toString(id));
        try {
            while(rs.next()) {
                chapterName = rs.getString(1);
            }
        } catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
        }
        return chapterName;
    }
    
    private int getChapterWeightageFromChapterId(int id) {
        String chapterName = getChapterNameFromChapterId(id);
        for(String key: weightageValue.keySet()) {
            if(key.equals(chapterName)) {
                return Integer.parseInt(weightageValue.get(key));
            }
        }
        return -1;
    }
    
    private int setProbability() {
        List<String> colName = new ArrayList<>();
        boolean result = false;
        colName.add("probability");
        for(int i = 0; i < questions.size(); i++) {
            List<String> colVal = new ArrayList<>();
            colVal.add(Integer.toString(getPreviousProbability(questions.get(i)) + 1));
            result = Database.update(colName, colVal, "questions", "id", questions.get(i).getId());
        }
        return result ? 1 : -1;
    }
    
    private int getPreviousProbability(Question q) {
        return (int)q.getProbability();
    }
    
    private int getQuestions() {
        for(int i = 0; i < chapterList.size(); i++) {
            selectedMarks = 0;
            rs = Database.select("SELECT SUM(marks) FROM questions WHERE chapter_id = " + Integer.toString(chapterList.get(i)) + " AND difficulty = " + difficultyLevel);
            try {
                while(rs.next()) {
                    if(getChapterWeightageFromChapterId(chapterList.get(i)) > rs.getInt(1)) {
                       JOptionPane.showMessageDialog(null, "Please add more Questions for chapter- " + getChapterNameFromChapterId(chapterList.get(i)));
                       return -1;
                    }
                }
            } catch(SQLException e) {
                JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
            }
            rs = Database.select("SELECT id, marks, question, chapter_id, isMCQType, difficulty, probability FROM questions WHERE chapter_id = " + chapterList.get(i) + " AND difficulty = " + difficultyLevel + " ORDER BY probability");
            try {
                while(rs.next()) {
                    Question question = new Question(rs.getInt(1), rs.getInt(4), rs.getString(3), rs.getInt(2), rs.getBoolean(5), rs.getInt(6), rs.getInt(7));
                    selectedMarks += rs.getInt(2);
                    questions.add(question);
                    if(selectedMarks >= getChapterWeightageFromChapterId(chapterList.get(i))) {
                        totalMarks = selectedMarks - rs.getInt(2);
                        if(selectedMarks > getChapterWeightageFromChapterId(chapterList.get(i))) {
                            Question q = questions.get(questions.size() - 1);
                            q.setMarks(getChapterWeightageFromChapterId(chapterList.get(i)) - totalMarks);
                        }
                        break;
                    }
                }
            } catch(SQLException e) {
                JOptionPane.showMessageDialog(null, "Exception : " + e, "Error", JOptionPane.OK_OPTION);
            }
        }
        return 1;
    }
}

class QuestionCompareByMarks implements Comparator<Question> {
    @Override
    public int compare(Question q1, Question q2) {
        if (q1.getMarks() < q2.getMarks()) return -1;
        if (q1.getMarks() > q2.getMarks()) return 1;
        else return 0;
    }
}

class QuestionCompareByDifficulty implements Comparator<Question> {
    @Override
    public int compare(Question q1, Question q2) {
        if (q1.getDifficulty() < q2.getDifficulty()) return -1;
        if (q1.getDifficulty() > q2.getDifficulty()) return 1;
        else return 0;
    }
}
