/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Keval Sanghvi
 */
public class Question {
    private int id;
    private int chapterId;
    private String question;
    private int marks;
    private boolean isMCQ;
    private int difficulty;
    private int probability;
    public Question(int id, int chapterId, String question, int marks, boolean isMCQ, int difficulty, int probability) {
        this.id = id;
        this.chapterId = chapterId;
        this.question = question;
        this.marks = marks;
        this.isMCQ = isMCQ;
        this.difficulty = difficulty;
        this.probability = probability;
    }
    
    public int getId() {
        return this.id;
    }
    
    public int getChapterId() {
        return this.chapterId;
    }
    
    public boolean getIsMCQ() {
        return this.isMCQ;
    }
    
    public int getMarks() {
        return this.marks;
    }
    
    public String getQuestion() {
        return this.question;
    }
    
    public int getDifficulty() {
        return this.difficulty;
    }
    
    public int getProbability() {
        return this.probability;
    }
    
    @Override
    public String toString() {
        return this.id + this.question + this.marks + "   " +  this.difficulty;
    }
    
    public void setMarks(int marks) {
        this.marks = marks;
    }
}